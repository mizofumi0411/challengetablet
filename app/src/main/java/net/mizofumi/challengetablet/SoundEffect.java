package net.mizofumi.challengetablet;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

/**
 * Created by mizof on 2016/02/17.
 */
public class SoundEffect {

    SoundPool soundPool;
    private final MediaPlayer ats1;
    private final MediaPlayer ats2;
    private final int click;

    private float volume = 1.0f;

    public SoundEffect(Context context) {
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        // 効果音をロードしておく。
        // 引数はContext、リソースID、優先度
        click = soundPool.load(context, R.raw.click, 1);
        ats1 = MediaPlayer.create(context, R.raw.ats1);
        ats2 = MediaPlayer.create(context, R.raw.ats2);
    }

    public void click(){
        soundPool.play(click, volume, volume, 0, 0, 1);
    }

    public void atsstart(){
        ats1.start();
        ats2.setVolume(volume/2,volume/2);
        ats2.setLooping(true);
        ats2.start();
    }

    public void atsstop(){
        if (ats1.isPlaying())
            ats1.stop();
        if (ats2.isPlaying())
            ats2.stop();
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public float getVolume() {
        return volume;
    }
}