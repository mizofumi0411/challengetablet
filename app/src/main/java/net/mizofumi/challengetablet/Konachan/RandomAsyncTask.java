package net.mizofumi.challengetablet.Konachan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

/**
 * Created by mizof on 2016/02/17.
 */
public class RandomAsyncTask extends AsyncTask<Void,Void,Void> {

    URL url;
    GetImageListener listener;
    private Bitmap bitmap;

    public RandomAsyncTask(GetImageListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onStart();
    }

    @Override
    protected Void doInBackground(Void... params) {
        String randomurl = "https://konachan.com/post/random";
        try {
            Connection.Response response = Jsoup.connect(randomurl).followRedirects(true).execute();
            url = response.url();

            Document document = Jsoup.connect(url.toString()).get();
            String imageurl = document.getElementsByClass("image").attr("src");
            Log.d("RandomAsyncTask", imageurl);

            bitmap = BitmapFactory.decodeStream(new URL(imageurl).openStream());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("RandomAsyncTask", "Error");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        listener.onGet(url,bitmap);
    }
}
