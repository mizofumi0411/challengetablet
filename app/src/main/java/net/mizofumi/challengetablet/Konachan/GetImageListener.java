package net.mizofumi.challengetablet.Konachan;

import android.graphics.Bitmap;

import java.net.URL;

/**
 * Created by mizof on 2016/02/17.
 */
public interface GetImageListener {

    void onStart();
    void onGet(URL url, Bitmap bitmap);

}
