package net.mizofumi.challengetablet;

import static java.lang.Math.*;
/**
 * Created by mizof on 2016/02/17.
 */
public class Earthquake {
    boolean sokuhou;
    String locale;
    String magnitude;
    String sindo;

    public Earthquake(String source){
        String[] info = source.split(",");
        System.out.println(String.valueOf(info[12])); //sindo
        System.out.println(String.valueOf(info[11])); //m
        System.out.println(String.valueOf(info[9])); //singen
        System.out.println(String.valueOf(info[14])); //sokuhou

        locale = info[9];
        magnitude = info[11];
        sindo = info[12];

        if (info[14].equals("0")){
            sokuhou = false;
        }else {
            if (getKyori(info[7],info[8]) < 120){
                sokuhou = true;
            }else {
                if (info[12].equals("5弱")||info[12].matches("5強")||info[12].equals("6弱")||info[12].matches("6強")||info[12].equals("7")){
                    sokuhou = true;
                }else {
                    sokuhou = false;
                }
            }
        }
    }

    public boolean isSokuhou() {
        return sokuhou;
    }

    public String getLocale() {
        return locale;
    }

    public String getMagnitude() {
        return magnitude;
    }

    public String getSindo() {
        return sindo;
    }

    protected double getKyori(String lat,String lng){
        double r = 6378.137; // 赤道半径[km]
        // mimomi
        double lat1 = 35.68781 * PI / 180;
        double lng1 = 140.0589923 * PI / 180;

        // shingen
        double lat2 = Double.valueOf(lat) * PI / 180;
        double lng2 = Double.valueOf(lng) * PI / 180;

        // 2点間の距離[km]
        double distance = r * acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lng2 - lng1));

        System.out.printf("%f km\n", distance);
        if (distance < 120){
            System.out.println("近いよ");
        }else {
            System.out.println("遠いよ");
        }
        return distance;
    }
}
