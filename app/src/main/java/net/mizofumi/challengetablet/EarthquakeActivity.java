package net.mizofumi.challengetablet;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import net.mizofumi.challengetablet.Twitter.TwitterUtlis;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

public class EarthquakeActivity extends Activity {
    int c = 30;
    boolean threadKill = false;
    SoundEffect soundEffect;
    TextView title;
    TextView count;
    TextView place;
    TextView scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake);
        title = (TextView)findViewById(R.id.title);
        place = (TextView)findViewById(R.id.place);
        place.setText(getIntent().getStringExtra("locale"));
        scale = (TextView)findViewById(R.id.scale);
        scale.setText(getIntent().getStringExtra("sindo"));
        count = (TextView)findViewById(R.id.count);

        soundEffect = new SoundEffect(this);
        soundEffect.atsstart();

        TwitterUtlis.twitterStream.clearListeners();
        TwitterUtlis.twitterStream.addListener(new StatusListener() {
            @Override
            public void onStatus(Status status) {
                if (status.getText().contains("test")) {

                }else {
                    final Earthquake earthquake = new Earthquake(status.getText());
                    if (earthquake.sokuhou){
                        c = 30;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                place.setText(earthquake.locale);
                                scale.setText(earthquake.sindo);
                            }
                        });
                    }
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {

            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {

            }

            @Override
            public void onStallWarning(StallWarning warning) {

            }

            @Override
            public void onException(Exception ex) {

            }
        });
        (findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Thread thread = new Thread(new Runnable() {
            boolean tmp = true;
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (tmp){
                                title.setTextColor(Color.parseColor("#ff0000"));
                                title.setBackgroundColor(Color.parseColor("#000000"));
                                tmp = false;
                            }else {
                                title.setTextColor(Color.parseColor("#000000"));
                                title.setBackgroundColor(Color.parseColor("#ff0000"));
                                tmp = true;
                            }
                        }
                    });
                    if (threadKill)
                        break;
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
        Thread countThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (c < 0){
                                finish();
                            }else {
                                count.setText(c+"秒後に閉じます");
                            }
                            c--;
                        }
                    });
                    if (threadKill)
                        break;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        countThread.start();
    }

    @Override
    public void finish() {
        super.finish();
        MainActivity.earthquakeshow = false;
        threadKill = true;
        soundEffect.atsstop();
        overridePendingTransition(0,0);
    }
}
