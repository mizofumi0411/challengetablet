package net.mizofumi.challengetablet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Bundle;
import android.os.Looper;
import android.text.format.Time;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.challengetablet.Konachan.GetImageListener;
import net.mizofumi.challengetablet.Konachan.RandomAsyncTask;
import net.mizofumi.challengetablet.Twitter.TwitterUtlis;

import java.net.URL;
import java.util.Calendar;

import twitter4j.ConnectionLifeCycleListener;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class MainActivity extends Activity {
    int COUNTDOWN_VALUE = 60;
    int count = COUNTDOWN_VALUE;
    ImageView imageView;
    TextView imagestatus;
    TextView date;
    TextView time;
    TextView twitter;
    RandomAsyncTask randomAsyncTask;
    Thread clockThread;

    Thread imageThread;
    Handler mHandler;
    public static boolean earthquakeshow = false;
    boolean threadKill = false;
    boolean downloading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //消灯させない

        mHandler = new Handler();
        imageView = (ImageView)findViewById(R.id.imageView);
        imagestatus = (TextView)findViewById(R.id.imagestatus);
        date = (TextView)findViewById(R.id.date);
        time = (TextView)findViewById(R.id.time);
        twitter = (TextView)findViewById(R.id.twitter);

        (findViewById(R.id.imageView)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImage();
            }
        });

        Configuration configuration = new ConfigurationBuilder().setOAuthConsumerKey(TwitterUtlis.CONSUMER_KEY)
                .setOAuthConsumerSecret(TwitterUtlis.CONSUMER_SECRET)
                .setOAuthAccessToken(TwitterUtlis.ACCESS_TOKEN)
                .setOAuthAccessTokenSecret(TwitterUtlis.ACCESS_TOKEN_SECRET)
                .build();
        TwitterUtlis.twitterStream = new TwitterStreamFactory(configuration).getInstance();
        TwitterUtlis.twitterStream.addConnectionLifeCycleListener(new ConnectionLifeCycleListener() {
            @Override
            public void onConnect() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        twitter.setText("接続中");
                        twitter.setTextColor(Color.parseColor("#00ffff"));
                    }
                });
            }

            @Override
            public void onDisconnect() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        twitter.setText("未接続");
                        twitter.setTextColor(Color.parseColor("#ff00ff"));
                    }
                });
            }

            @Override
            public void onCleanUp() {

            }
        });
        getImage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        threadKill = false;
        clockThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Calendar calendar = Calendar.getInstance();
                            String weekDay="日,月,火,水,木,金,土";
                            String[] dayStr = weekDay.split(",");
                            Time times = new Time();
                            times.setToNow();
                            String nowdate = String.format("%04d/%02d/%02d (%s曜日)", times.year, (times.month + 1), times.monthDay,dayStr[calendar.get(Calendar.DAY_OF_WEEK)-1]);
                            String nowtime = String.format("%02d:%02d:%02d", times.hour, times.minute, times.second);
                            date.setText(nowdate);
                            time.setText(nowtime);
                        }
                    });
                    if (threadKill) break;
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        imageThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (count<=0){
                                getImage();
                            }else {
                                if (!downloading) imagestatus.setText(count+"秒後更新");
                            }
                            count--;
                        }
                    });
                    if (threadKill) break;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        TwitterUtlis.twitterStream.addListener(new StatusListener() {
            @Override
            public void onStatus(Status status) {
                if (earthquakeshow)
                    return;
                Earthquake earthquake = new Earthquake(status.getText());
                if (earthquake.sokuhou){
                    Intent intent = new Intent(MainActivity.this, EarthquakeActivity.class);
                    intent.putExtra("locale",earthquake.locale);
                    intent.putExtra("sindo",earthquake.sindo);
                    startActivity(intent);
                    earthquakeshow = true;
                    overridePendingTransition(0, 0);
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {

            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {

            }

            @Override
            public void onStallWarning(StallWarning warning) {

            }

            @Override
            public void onException(Exception ex) {

            }
        });
        clockThread.start();
        imageThread.start();
        if (twitter.getText().toString().contains("- - -")){
            TwitterUtlis.twitterStream.user();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        threadKill = true;
        TwitterUtlis.twitterStream.shutdown();
    }

    private void getImage(){
        randomAsyncTask = new RandomAsyncTask(new GetImageListener() {
            @Override
            public void onStart() {
                downloading = true;
                imagestatus.setText("画像取得中...");
            }

            @Override
            public void onGet(URL url, Bitmap bitmap) {
                if (bitmap != null){
                    imageView.setImageBitmap(bitmap);
                    imagestatus.setText("取得完了");
                }else {
                    imagestatus.setText("取得失敗");
                }
                count = COUNTDOWN_VALUE;
                downloading = false;
            }
        });
        if (!downloading)
            randomAsyncTask.execute();
    }
}
